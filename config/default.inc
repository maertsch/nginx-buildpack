# set cache-header for images
location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
  expires 1y;
  add_header Cache-Control public;
  access_log off;
}

# No missing favicon.ico log
location = /favicon.ico { access_log off; log_not_found off; }
location = /robots.txt { access_log off; log_not_found off; }

# protect .git
location ~ /\.git { deny all; }

# secure lucee admin
location ~ ^/lucee/admin { deny all; }
